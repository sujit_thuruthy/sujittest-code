public static async Task Run(Stream myBlob, string name, Stream outputBlob, TraceWriter log)
{
try {
        // Copy BLOB from one Storage Account Container to another container
        using (MemoryStream ms = new MemoryStream())
        {
            myBlob.CopyTo(ms);
            var byteArray = ms.ToArray();
            await outputBlob.WriteAsync(byteArray, 0, byteArray.Length);
            log.Info("Copy completed");
        }
    }    
    catch(Exception ex){
        log.Error(ex.Message);
        log.Info("Copy failed");
    }
    finally{
        log.Info("Operation completed");
    }
}
