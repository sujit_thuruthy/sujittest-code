﻿using System.IO;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs.Extensions.Http;

namespace AzureBlobCopyTrig
{
    public static class FnCopyBlob
    {
        [FunctionName("BlobTriggerCSharp")]        
        public static void Run([BlobTrigger("tst-trig-src/{name}.txt.gz", Connection = "wwaustoragetest_STORAGE")]Stream myBlob, string name, [Blob("tst-trig-dest/{name}.txt.gz", Connection = "lsmstoragetest_STORAGE")]Stream outputBlob, TraceWriter log)
        {
            log.Info($"C# Blob trigger function Processed blob\n Name:{name} \n Size: {myBlob.Length} Bytes");
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    myBlob.CopyTo(ms);
                    var byteArray = ms.ToArray();
                    outputBlob.WriteAsync(byteArray, 0, byteArray.Length);
                    log.Info("This is added to the code");
                    log.Info($"Copy Completed : {name}");
                }
                
            }
            catch(System.Exception ex)
            {
                log.Error(ex.Message);
                log.Info($"Copy Failed  : {name}");
            }
            finally
            {
                log.Info("Copy process complete");
            }
        }
    }
}